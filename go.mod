module codeberg.org/lig/wsq

go 1.22

require (
	github.com/google/uuid v1.6.0
	github.com/gorilla/websocket v1.5.1
	github.com/nsqio/go-nsq v1.1.0
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	golang.org/x/net v0.24.0 // indirect
)
