# WSQ

[![Go Reference](https://pkg.go.dev/badge/codeberg.org/lig/wsq/pkg/wsq.svg)](https://pkg.go.dev/codeberg.org/lig/wsq/pkg/wsq)

WSQ allows web clients to use [NSQ (a realtime distributed messaging platform)](https://nsq.io/) over websocket connections and communicate with any other service that uses the same NSQ cluster.

WSQ is licensed under [MIT License](LICENSE).

## Features

- Write to a topic
- Subscribe to a topic and read from it
- Support arbitrary serialization on the NSQ side
- Support arbitrary serialization on the Websocket side
- Authentication
- Filter incoming messages

## How to contribute

[WSQ project on Codeberg](https://codeberg.org/lig/wsq)

Contributions in any form are always welcome. Feel free to report issues, provide feedback, suggest features, and submit pull requests.


## Running the Demo

Prerequisites:
- Have [Bash](https://www.gnu.org/software/bash/) installed in your system.
- Install [Go](https://go.dev/doc/install).
- Install [Flutter](https://docs.flutter.dev/get-started/install).
- Install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

Execute the following command:

```shell
./_demo/run
```

Your preferred browser should open automatically. Let the demo UI some time to load after that.
