import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:provider/provider.dart';

import 'package:flutter/material.dart';

import 'models.dart' as models;

class DemoUI extends StatelessWidget {
  const DemoUI({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(title: const Text("WSQ Demo Chat")),
        body: Container(
          alignment: Alignment.topCenter,
          padding: const EdgeInsets.symmetric(vertical: 20),
          child: Container(
            constraints: const BoxConstraints(maxWidth: 1000),
            child: const Chat(),
          ),
        ),
      );
}

class Chat extends StatelessWidget {
  const Chat({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => DecoratedBox(
        decoration: const BoxDecoration(
          border: Border.fromBorderSide(
            BorderSide(color: Colors.black),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Container(
                constraints: const BoxConstraints(maxWidth: 200),
                child: const Roster(),
              ),
              const Padding(padding: EdgeInsets.all(4.0)),
              Expanded(
                child: Column(
                  children: const [
                    ErrorPane(),
                    Expanded(child: Room()),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}

class Roster extends StatelessWidget {
  const Roster({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            const Text("Name: "),
            Expanded(
              child: Selector<models.Chat, String>(
                selector: (context, chat) => chat.username,
                builder: (context, username, child) => TextFormField(
                  initialValue: username,
                  onChanged: (value) =>
                      context.read<models.Chat>().setUsername(value),
                ),
              ),
            ),
          ],
        ),
        const Divider(),
        Row(
          children: [
            const Text("Room: "),
            Expanded(
              child: TextFormField(
                onFieldSubmitted: (value) =>
                    context.read<models.Chat>().setCurrentRoom(value),
              ),
            ),
          ],
        ),
        const Divider(),
        Expanded(
          child: Selector<models.Chat, Set<String>>(
            selector: (context, chat) => chat.rooms,
            builder: (context, rooms, child) => ListView(
              children: rooms
                  .map(
                    (room) => RoomTile(room: room),
                  )
                  .toList(),
            ),
          ),
        )
      ],
    );
  }
}

class RoomTile extends StatelessWidget {
  final String room;

  const RoomTile({
    Key? key,
    required this.room,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Selector<models.Chat, String?>(
      selector: (context, chat) => chat.currentRoom,
      shouldRebuild: (prev, next) =>
          prev != next && {prev, next}.contains(room),
      builder: (context, currentRoom, child) => ListTile(
        key: Key(room),
        selected: room == currentRoom,
        selectedTileColor: Colors.blueGrey[100]!,
        onTap: () => context.read<models.Chat>().setCurrentRoom(room),
        title: Text(room),
      ),
    );
  }
}

class ErrorPane extends StatelessWidget {
  const ErrorPane({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) =>
      Selector<models.Chat, List<models.ErrorEntry>>(
        selector: (context, chat) => chat.errors,
        builder: (context, errors, child) => Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: errors
              .map((entry) => Text(
                    "${entry.code}: ${entry.description}",
                    style: const TextStyle(backgroundColor: Colors.red),
                  ))
              .toList(),
        ),
      );
}

class Room extends StatelessWidget {
  const Room({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Selector<models.Chat, String?>(
        selector: (context, chat) => chat.currentRoom,
        builder: (context, currentRoom, child) =>
            currentRoom != null ? child! : Column(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const ChatLog(),
            const Divider(),
            MessageInput(),
          ],
        ),
      );
}

class ChatLog extends StatelessWidget {
  const ChatLog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: DecoratedBox(
        decoration: const BoxDecoration(
          border: Border.fromBorderSide(
            BorderSide(color: Colors.black),
          ),
        ),
        child: Selector<models.Chat, List<models.HistoryEntry>>(
          selector: (context, chat) => chat.currentRoomHistory,
          builder: (context, history, child) => Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: history
                .map((entry) => Text("${entry.author}: ${entry.message}"))
                .toList(),
          ),
        ),
      ),
    );
  }
}

class MessageInput extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();

  MessageInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Function() submit;
    submit = () {
      if (_formKey.currentState!.saveAndValidate()) {
        context
            .read<models.Chat>()
            .sendToCurrentRoom(_formKey.currentState!.value['message']);
        _formKey.currentState!.reset();
      }
    };

    return FormBuilder(
      key: _formKey,
      child: Row(
        children: [
          Expanded(
            child: FormBuilderTextField(
              name: 'message',
              validator: FormBuilderValidators.required(),
              onSubmitted: (value) => submit(),
            ),
          ),
          IconButton(
            icon: const Icon(Icons.send),
            onPressed: submit,
          )
        ],
      ),
    );
  }
}
