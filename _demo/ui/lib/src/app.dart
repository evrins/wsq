import 'package:flutter/material.dart';

import 'views.dart' as views;

class DemoUIApp extends StatelessWidget {
  const DemoUIApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WSQ Demo UI',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const views.DemoUI(),
    );
  }
}
