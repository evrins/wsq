import 'dart:convert';
import 'dart:math';

import 'package:web_socket_channel/web_socket_channel.dart';

import 'package:flutter/foundation.dart';

class Chat extends ChangeNotifier {
  String _username = Chat._generateUsername();
  final Set<String> _rooms = {};
  String? _currentRoom;
  final WebSocketChannel _wsChannel =
      WebSocketChannel.connect(Uri.parse("ws://localhost:9980/"));
  final Map<String, List<HistoryEntry>> _chatHistory = {};
  final List<ErrorEntry> _errors = [];

  Chat() {
    _wsChannel.stream.listen(_onWSData);
  }

  String get username => _username;
  setUsername(String value) {
    _username = value;
    notifyListeners();
  }

  Set<String> get rooms => Set.unmodifiable(_rooms);
  String? get currentRoom => _currentRoom;
  setCurrentRoom(String room) {
    if (room == _currentRoom) return;

    if (_rooms.add(room)) _wsChannel.sink.add("sub $room");

    _currentRoom = room;
    notifyListeners();
  }

  List<HistoryEntry> get currentRoomHistory =>
      List.unmodifiable(_chatHistory[currentRoom] ?? []);

  List<ErrorEntry> get errors => List.unmodifiable(_errors);

  void sendToCurrentRoom(String message) {
    if (currentRoom == null) return;

    String encodedMessage =
        json.encode({'author': username, 'message': message});
    _wsChannel.sink.add("pub $currentRoom $encodedMessage");
  }

  void _onWSData(dynamic rawData) {
    String messageData = utf8.decode(rawData);

    if (messageData.startsWith("#err ")) {
      _onWSError(messageData.substring(4));
      return;
    }

    _onWSMessage(messageData);
  }

  void _onWSMessage(String messageData) {
    int topicSplitIndex = messageData.indexOf(" ");

    String topic = messageData.substring(0, topicSplitIndex);
    String encodedMessage = messageData.substring(topicSplitIndex + 1);
    Map<String, String> entryData = json.decode(encodedMessage);

    _chatHistory.putIfAbsent(topic, () => []).add(HistoryEntry(
          author: entryData['author'] ?? '',
          message: entryData['message'] ?? '',
        ));

    notifyListeners();
  }

  void _onWSError(String errorData) {
    int codeSplitIndex = errorData.indexOf(" ");

    String code = errorData.substring(0, codeSplitIndex);
    String description = errorData.substring(codeSplitIndex + 1);

    _errors.add(ErrorEntry(code: code, description: description));

    notifyListeners();
  }

  static String _generateUsername() {
    return "user${Random().nextInt(1000)}";
  }
}

class HistoryEntry {
  final String author;
  final String message;

  HistoryEntry({
    required this.author,
    required this.message,
  });
}

class ErrorEntry {
  final String code;
  final String description;

  ErrorEntry({
    required this.code,
    required this.description,
  });
}
