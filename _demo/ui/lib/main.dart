import 'package:provider/provider.dart';

import 'package:flutter/material.dart';

import 'src/app.dart';
import 'src/models.dart' as models;

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => models.Chat(),
      child: const DemoUIApp(),
    ),
  );
}
