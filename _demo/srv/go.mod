module codeberg.org/lig/wsq/_demo/srv

go 1.22

require (
	codeberg.org/lig/wsq v0.5.0
	github.com/fxamacker/cbor/v2 v2.6.0
	github.com/nsqio/go-nsq v1.1.0
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	golang.org/x/net v0.24.0 // indirect
)
