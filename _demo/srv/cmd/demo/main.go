package main

import (
	"codeberg.org/lig/wsq/pkg/wsq"
	"encoding/json"
	"github.com/fxamacker/cbor/v2"
	"github.com/nsqio/go-nsq"
)

func main() {
	wsqConfig := wsq.NewConfig()
	wsqConfig.SetWSCheckOrigin(wsq.CheckOriginBypass)

	// server := wsq.NewDefaultServer(":9980")
	server := wsq.NewServer[message, *wsq.AnonymousUser](
		":9980",
		wsqConfig,
		nsq.NewConfig(),
		&wsq.Transformer[message]{NSQEnDec: &nsqEnDec{}, WSEnDec: &wsEnDec{}},
		&wsq.NoAuthentication,
	)
	server.Run()
}

type message map[string]interface{}

type nsqEnDec struct{}

func (ed *nsqEnDec) Marshal(topic string, msg message) ([]byte, error) {
	result, err := cbor.Marshal(msg)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (ed *nsqEnDec) UnMarshal(topic string, data []byte) (message, error) {
	msg := make(map[string]interface{})
	err := cbor.Unmarshal(data, &msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}

type wsEnDec struct{}

func (ed *wsEnDec) Marshal(topic string, msg message) ([]byte, error) {
	result, err := json.Marshal(msg)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (ed *wsEnDec) UnMarshal(topic string, data []byte) (message, error) {
	msg := make(map[string]interface{})
	err := json.Unmarshal(data, &msg)
	if err != nil {
		return nil, err
	}
	return msg, nil
}
