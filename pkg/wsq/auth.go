package wsq

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/websocket"
)

type AuthenticationError struct {
	Message string
}

func (e *AuthenticationError) Error() string {
	return fmt.Sprintf("Authentication Error: %s", e.Message)
}

/*
Authenticator defines interface for implementing authentication in WSQ.

This is a generic interface. The implementation must be instantiated using type `U`
that must implement the [User] interface.
*/
type Authenticator[U User] interface {
	// Authenticate receives [websocket.Conn] and [http.Request], can communicate on
	// websocket.
	// Must return [User] on successful authentication.
	// Must return [AuthenticationError] in case of authentication error.
	// Should not close websocket connection.
	Authenticate(wsConn wsConnI, r *http.Request) (U, error)
}

type User interface {
	// IsAuthenticated returns user's authentication status. This method is provided for
	// convenience only. Other User methods could utilize IsAuthenticated result. There
	// is no internal requirement for it to return `true` for user to be granted
	// access to communicating with NSQ.
	IsAuthenticated() bool
	// CanPub grants user access to publishing to the specified topic.
	CanPub(topic string) bool
	// CanSub grants user access to subscribing to the specified topic.
	CanSub(topic string) bool
	// CanRead grants user access to receiving the specified message posted to the
	// specified topic.
	CanRead(topic string, message any) bool
}

/*
AnonymousAuthenticator is an Authenticator implementation that allows full anonymous
access to a WSQ server without authentication.
*/
type AnonymousAuthenticator struct{}

func (a *AnonymousAuthenticator) Authenticate(wsConn wsConnI, r *http.Request) (*AnonymousUser, error) {
	return NewAnonymousUser(true), nil
}

/*
AnonymousUser type implements [User] and can be used to allow full access or deny any
access, see [NewAnonymousUser].
*/
type AnonymousUser struct {
	grantAll bool
}

/*
NewAnonymousUser creates new [AnonymousUser] instance. Use `grantAll` argument to
control new instance behavior. If `grantAll` is `true` then all methods defined in [User]
return `true` and wise versa.
*/
func NewAnonymousUser(grantAll bool) *AnonymousUser {
	return &AnonymousUser{
		grantAll: grantAll,
	}
}

func (u *AnonymousUser) IsAuthenticated() bool {
	return u.grantAll
}

func (u *AnonymousUser) CanPub(topic string) bool {
	return u.grantAll
}

func (u *AnonymousUser) CanSub(topic string) bool {
	return u.grantAll
}

func (u *AnonymousUser) CanRead(topic string, message any) bool {
	return u.grantAll
}

/*
NoAuthentication is a default [AnonymousAuthenticator] instance provided for convenience.
*/
var NoAuthentication = AnonymousAuthenticator{}

/*
AuthMessageHandler is an interface defining AuthenticateViaAuthMessage method used in
[AuthMessageAuthenticator] for handling authentication message data.

This is a generic interface. The implementation must be instantiated using type `U`
which must implement [User].
*/
type AuthMessageHandler[U User] interface {
	AuthenticateViaAuthMessage(message []byte) (U, error)
}

/*
AuthMessageAuthenticator is a generic implementation of the [Authenticator] interface
that uses [AuthMessageHandler] implementation to authenticate websocket client using a
special `AUTH` command.

The `AUTH` websocket command has the form `AUTH <auth_data>`. AuthMessageAuthenticator
handles processing of this command, passes `<auth_data>` as is to the provided
[AuthMessageHandler.AuthenticateViaAuthMessage] implementation, and returns its result
as is.
*/
type AuthMessageAuthenticator[U User] struct {
	AuthMessageHandler[U]
}

func NewAuthMessageAuthenticator[U User](handler AuthMessageHandler[U]) *AuthMessageAuthenticator[U] {
	return &AuthMessageAuthenticator[U]{handler}
}

func (a *AuthMessageAuthenticator[U]) Authenticate(wsConn wsConnI, r *http.Request) (User, error) {
	_, rawData, err := wsConn.ReadMessage()
	if err != nil {
		if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
			log.Println(err)
		}
		return nil, err
	}

	data := rawData[:]
	cmd, cmdArgs, hasArgs := bytes.Cut(data, msgSep)

	switch strings.ToUpper(string(cmd)) {
	case "AUTH":
		if !hasArgs {
			errorMessage := "AUTH used without args"
			log.Println(errorMessage)
			return nil, &AuthenticationError{Message: errorMessage}
		}

		return a.AuthenticateViaAuthMessage(cmdArgs)
	}

	errorMessage := fmt.Sprintf("unknown command: `%s`", cmd)
	log.Println(errorMessage)
	return nil, &AuthenticationError{Message: errorMessage}
}
