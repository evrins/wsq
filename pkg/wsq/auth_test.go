package wsq

import (
	"testing"
)

type fakeAuthMessageHandler struct{}

func (h *fakeAuthMessageHandler) AuthenticateViaAuthMessage(message []byte) (*AnonymousUser, error) {
	switch string(message) {
	case "allow":
		return NewAnonymousUser(true), nil
	case "disallow":
		return NewAnonymousUser(false), nil
	}
	return nil, &AuthenticationError{Message: "error handling auth message"}
}

type testAuthMessageAuthenticatorData struct {
	name          string
	message       string
	authenticated bool
	errored       bool
}

func TestAuthMessageAuthenticator(t *testing.T) {
	authenticator := NewAuthMessageAuthenticator[*AnonymousUser](&fakeAuthMessageHandler{})
	wsConn := newFakeWSConn()

	for _, testData := range []testAuthMessageAuthenticatorData{
		{
			name:          "allow",
			message:       "AUTH allow",
			authenticated: true,
			errored:       false,
		},
		{
			name:          "disallow",
			message:       "AUTH disallow",
			authenticated: false,
			errored:       false,
		},
		{
			name:          "error from handler",
			message:       "AUTH other",
			authenticated: false,
			errored:       true,
		},
		{
			name:          "error on parsing",
			message:       "other",
			authenticated: false,
			errored:       true,
		},
	} {
		t.Run(testData.name, func(t *testing.T) {
			wsConn.messagesIn <- testData.message

			actualUser, err := authenticator.Authenticate(wsConn, nil)
			if testData.errored {
				if e, ok := err.(*AuthenticationError); !ok {
					t.Errorf("unexpected error `%s`", e)
				}
			} else {
				if err != nil {
					t.Log(err)
					t.FailNow()
				}

				if actualUser.IsAuthenticated() != testData.authenticated {
					t.Fail()
				}
			}
		})
	}
}
