/*
Package wsq is a lightweight framework for building a websocket server that communicates
with the NSQ messaging platform.

Simple Server

Creating the default server

	server := wsq.NewDefaultServer(":9980")
	server.Run()

This creates a websocket server listening on port "9980". The default server has no
authentication enabled and no marshaling configured. It connects to a default NSQ
instance address and relays messages as is. See [wsq.Config] docs for all default values
being used.

Full-featured Server

The default server isn't that useful in production.

Here is an example of `wsq.NewServer` usage

	// Create the default config
	wsqConfig := wsq.NewConfig()
	// Configure Websocket CheckOrigin callback to bypass any checks.
	// Don't do it in production!
	wsqConfig.SetWSCheckOrigin(wsq.CheckOriginBypass)

	// Create the server instance specifying message and user types to use.
	server := wsq.NewServer[message, *wsq.AnonymousUser](
		// Address (optional) and port to listen on
		":9980",
		// WSQ Config instance
		wsqConfig,
		// NSQ Config instance
		nsq.NewConfig(),
		// WSQ Transformer struct providing encoders/decoders for NSQ and Websocket sides respectivly
		&wsq.Transformer[message]{NSQEnDec: &nsqEnDec{}, WSEnDec: &wsEnDec{}},
		// Authentication controller
		&wsq.NoAuthentication,
	)
	server.Run()

See [wsq.NewServer] for detailed documentation on creating WSQ Server.

See full server example in the demo directory of the WSQ repo:
https://codeberg.org/lig/wsq/src/branch/main/_demo/srv/cmd/demo/main.go

Protocol

WSQ uses a protocol somewhat similar to NSQ protocol to communicate with a client
connected over the websocket.

Each websocket message from the client to WSQ has the following form:

	<command> <args>

where
	- <command> -- one of `SUB`, `USUB`, `PUB`;
	- <args> -- argument list depending on the command.

Commands:
	SUB <topicName>

		<topicName> -- The Name of the NSQ topic to subscribe.

	USUB <topicName>

		<topicName> -- The Name of the NSQ topic to unsubscribe.

	PUB <topicName> <message>

		<topicName> -- The Name of the NSQ topic to publish message to;
		<message> -- The message to publish.

The <message> provided in `PUB` command is subject to marshaling, see ``Message Encoding
and Decoding'' below.

There are two types of websocket messages from WSQ to the client:
	- normal message;
	- error message.

The normal websocket message from WSQ to the client has the following form:

	<topicName> <message>

where
	- <topicName> -- the name of the NSQ topic of the received message;
	- <message> -- the message received from NSQ.

The <message> here is subject to marshaling, see ``Message Encoding and Decoding'' below.

The error websocket message from WSQ to the client has the following form:

	#err <code> <description>

where
	- <code> -- the 3-digit code representing the error type similar to HTTP response codes;
	- <description> -- human readable error description useful for debugging.

As of the time of writing this documentation there are 3 error codes defined in WSQ:
	- 400 -- errors related to malformed client commands;
	- 403 -- errors related to unauthorized access to some actions;
	- 500 -- errors related to communicating to NSQ or other server-related errors.

Message Encoding and Decoding

WSQ provides an ability to marshal and unmarshal messages on NSQ and Websocket sides.
For each side a type implementing the [wsq.EnDec] interface must be defined.
A message received from NSQ will be unmarshalled using `[EnDec].UnMarshal` defined for the
NSQ side and marshaled using `[EnDec].Marshal` defined for the Websocket side before being
send to a client connected over the websocket. The same works in the opposite direction
with corresponding EnDecs being used for unmarshalling and marshalling.

Authentication and authorization

WSQ provides [wsq.Authenticator] and [wsq.User] interfaces that must be implemented for
authentication and authorization to work respectively. It is up to developer how to
implement those interfaces.

The `[Authenticator].Authenticate` method is being called before WSQ starts any client
command processing. The method must return a valid [User] instance for WSQ to accept the
client. There is no requirement for `[User].IsAuthenticated` method to return `true` if
called.

For convenience WSQ provides the [AuthMessageAuthenticator] type and the
[AuthMessageHandler] interface that allow to implement authentication using a special
`AUTH` client command.

The [User] interface is being used for authorizing access to subscribing and publishing
for a specific topic.

Message filtering

WSQ provides an ability to filter incoming messages basing on the NSQ topic name and
message content. The [User] interface provides the `CanRead` method for this purpose.
*/
package wsq
